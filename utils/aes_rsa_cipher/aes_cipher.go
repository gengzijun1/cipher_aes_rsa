package aes_rsa

import (
	"encoding/base64"
	"fmt"
	"lizi/utils"
)

//加密
func AesEncryption(Password string, Aeskey string) string {
	xpass, err := utils.AesEncrypt([]byte(Password), []byte(Aeskey))
	if err != nil {
		fmt.Println(err)
		// return
	}
	pass64 := base64.StdEncoding.EncodeToString(xpass)
	fmt.Printf("加密后:%v\n", pass64)
	bytesPass, err := base64.StdEncoding.DecodeString(pass64)
	if err != nil {
		fmt.Println(err)
		// return
	}
	return string(bytesPass)
}

//解密
func AesDecrypt(bytesPass string, Aeskey string) string {
	tpass, err := utils.AesDecrypt([]byte(bytesPass), []byte(Aeskey))
	if err != nil {
		fmt.Println(err)
		// return
	}
	fmt.Printf("解密后:%s\n", tpass)
	return string(tpass)
}
