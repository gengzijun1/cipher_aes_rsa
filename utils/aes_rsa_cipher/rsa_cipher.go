package aes_rsa

import (
	"encoding/base64"
	"fmt"
	"lizi/utils"
)

//加密
func RSACipher(x []byte, PublicKey string, privatekey string) {
	x1, err := utils.EncryptRSAPublic([]byte(x), PublicKey)
	if err != nil {
		fmt.Print(err)
		return
	}
	encodeString := base64.StdEncoding.EncodeToString(x1)
	fmt.Println("加密：", encodeString)

	x2, err := utils.DecryptRSAPrivate(x1, privatekey)
	if err != nil {
		fmt.Print(err)
		return
	}
	fmt.Println("解密：", string(x2))
}
