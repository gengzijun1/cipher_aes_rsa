package utils

import (
	"lizi/config"
	"os"

	"gopkg.in/yaml.v2"
)

func ParseYaml(c *config.Config, path string) error {
	if f, err := os.Open(path); err != nil {
		return err
	} else {
		yaml.NewDecoder(f).Decode(c)
	}
	return nil
}
