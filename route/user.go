package route

import (
	"lizi/controller"

	"github.com/gin-gonic/gin"
)

func User(router gin.Engine) *gin.Engine {
	v1 := router.Group("/apis/v1/")
	{
		v1.POST("/register", controller.RegisterUser)
		v1.POST("/login", controller.Login)
	}
	return &router
}
