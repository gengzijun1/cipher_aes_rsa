package route

import (
	"lizi/middleware"
	md "lizi/middleware"
	"lizi/response"

	"github.com/gin-gonic/gin"
)

// 测试一个需要认证的接口
func GetDataByTime(c *gin.Context) {
	claims := c.MustGet("claims").(*middleware.CustomClaims)
	if claims != nil {
		response.TestApi(claims, c)

	}
}

//因此该分组下的api都需要使用jwt-token认证
func TestApi(router gin.Engine) *gin.Engine {
	sv1 := router.Group("/apis/v1/auth/")
	sv1.Use(md.JWTAuth())
	{
		sv1.GET("/time", GetDataByTime)

	}
	return &router
}
