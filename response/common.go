package response

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type Response struct {
	Status int
	Msg    string
	Data   interface{}
}

//状态码
const (
	ERROR   = -1
	SUCCESS = 0
)

//消息
const (
	REGISTEROK   = "注册成功"
	REGISTERFAIL = "注册失败"
	LOGINFAIL    = "登录验证失败"
	LOGINOK      = "登录验证成功"
	TOKEN        = "token有效"
	TOKENEXPIRE  = "token授权已过期,请重新申请授权"
	NILTOKEN     = "请求未携带token,无权限访问"
	NIL          = "nil"
)

func Result(status int, msg string, data interface{}, c *gin.Context) {
	c.JSON(http.StatusOK, Response{
		status,
		msg,
		data,
	})
}

func Error_Nil(err string, c *gin.Context) {
	Result(ERROR, err, NIL, c)
}

func TestApi(claims interface{}, c *gin.Context) {
	Result(SUCCESS, TOKEN, claims, c)
}
