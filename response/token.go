package response

import (
	"github.com/gin-gonic/gin"
)

func TokenExpire(c *gin.Context) {
	Result(ERROR, TOKENEXPIRE, NIL, c)
}
func NilToken(c *gin.Context) {
	Result(ERROR, NILTOKEN, NIL, c)
}
