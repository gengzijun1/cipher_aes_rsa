package response

import (
	"github.com/gin-gonic/gin"
)

//消息

func RegisterOK(c *gin.Context) {
	Result(SUCCESS, REGISTEROK, NIL, c)
}

func RegisterFail(c *gin.Context) {
	Result(ERROR, REGISTERFAIL, NIL, c)
}

func LoginFail(c *gin.Context) {
	Result(ERROR, LOGINFAIL, NIL, c)
}

func LoginOK(token interface{}, c *gin.Context) {
	Result(SUCCESS, LOGINOK, token, c)
}
