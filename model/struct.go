package model

import (
	"time"
)

type User_Info struct {
	Id        int32  `gorm:"AUTO_INCREMENT"`
	Name      string `json:"name"`
	Pwd       string `json:"passwoTestApird"`
	Phone     int64  `gorm:"DEFAULT:0"`
	Email     string `gorm:"type:varchar(20);unique_index;"`
	CreatedAt *time.Time
	UpdateTAt *time.Time
}

// LoginReq请求参数
type LoginReq struct {
	Name string `json:"name"`
	Pwd  string `json:"password"`
}

// 用户注册信息
// 注意:注册信息可以使用Gin内部的校验工具或者beego的校验工具进行校验
type RegisterInfo struct {
	Phone int64  `json:"phone"`
	Name  string `json:"name"`
	Pwd   string `json:"password"`
	Email string `json:"email"`
}

// 登陆结果
type LoginResult struct {
	Token string `json:"token"`
}
