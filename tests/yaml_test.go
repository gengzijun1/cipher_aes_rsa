package tests

import (
	"fmt"
	"lizi/config"
	"testing"
)

func TestYaml(t *testing.T) {
	var c = &config.Config{}
	var path = "../config/config.yaml"
	config.Parse(c, path)
	fmt.Println(c)
}
