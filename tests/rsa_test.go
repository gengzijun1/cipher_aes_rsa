package tests

import (
	"encoding/base64"
	"fmt"
	"lizi/config"
	"lizi/utils"
	"lizi/utils/aes_rsa"
	"testing"
)

func Test_rsa(t *testing.T) {
	var c = &config.Config{}
	var path = "../config/config.yaml"
	config.Parse(c, path)
	b, _ := utils.EncryptRSAPublic([]byte(c.JWT.AES.Password), c.JWT.RSA.PublicKey)
	encodeString := base64.StdEncoding.EncodeToString(b)
	fmt.Printf("加密: %v\n", encodeString)
	b2, _ := utils.DecryptRSAPrivate(b, c.JWT.RSA.PrivateKey)
	fmt.Printf("解密: %v\n", string(b2))

}

func Test_Rsa(t *testing.T) {
	var c = &config.Config{}
	var path = "../config/config.yaml"
	config.Parse(c, path)
	aes_rsa.RSACipher([]byte(c.JWT.AES.Password), c.JWT.RSA.PublicKey, c.JWT.RSA.PrivateKey)
}
