package tests

import (
	// "lizi/aes_rsa_mian"
	// "lizi/config"

	"encoding/base64"
	"fmt"
	"lizi/config"
	"lizi/utils"
	"testing"
)

func TestAes(t *testing.T) {
	var c = &config.Config{}
	var path = "../config/config.yaml"
	config.Parse(c, path)
	xpass, _ := utils.AesEncrypt([]byte(c.JWT.AES.Password), []byte(c.JWT.AES.Aeskey))
	pass64 := base64.StdEncoding.EncodeToString(xpass)
	fmt.Printf("加密后:%v\n", pass64)
	bytesPass, _ := base64.StdEncoding.DecodeString(pass64)
	tpass, _ := utils.AesDecrypt(bytesPass, []byte(c.JWT.AES.Aeskey))
	fmt.Printf("解密后:%s\n", tpass)
}
