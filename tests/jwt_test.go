package tests

import (
	"encoding/json"
	"fmt"
	"lizi/service"
	"testing"
)

//生成token
func TestCreate(t *testing.T) {
	map1 := make(map[string]interface{})
	map1["1"] = "hello"
	map1["2"] = "world"

	//return []byte
	str, err := json.Marshal(map1)
	if err != nil {
		fmt.Println(err)
	}
	// var str = "123"
	fmt.Println(string(str))
	token := service.GenerateToken(string(str))
	fmt.Println(token)
}

//解析token
func TestParserToken(t *testing.T) {
	var token string = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjoie1wiMVwiOlwiaGVsbG9cIixcIjJcIjpcIndvcmxkXCJ9IiwiZXhwIjoxNjU2MDIxODM2LCJpc3MiOiJsaXppIiwibmJmIjoxNjU1OTg0ODM2fQ.Mmec6SMwb_eTtVrWjiFLbLkoLXYXf1jk9l-3B9wDEyY"
	str := service.ParseToken(token)
	fmt.Println(str)
	fmt.Printf("str: %T\n", str)
	//json([]byte) to map
	map2 := make(map[string]interface{})
	err := json.Unmarshal([]byte(str.(string)), &map2)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("json to map ", map2)
	fmt.Println("The value of key1 is", map2["1"])
}
