package controller

import (
	_ "fmt"
	"lizi/model"
	"lizi/response"

	"github.com/gin-gonic/gin"
)

// 用户注册接口
func RegisterUser(c *gin.Context) {
	var registerInfo model.RegisterInfo
	bindErr := c.BindJSON(&registerInfo)
	if bindErr == nil {
		// 用户注册
		err := model.Register(registerInfo.Name, registerInfo.Pwd, registerInfo.Phone, registerInfo.Email)
		if err == nil {
			response.RegisterOK(c)
		} else {
			response.RegisterFail(c)
		}
	} else {
		response.RegisterFail(c)
	}
}

// 登陆接口 用户名和密码登陆
// name,password
func Login(c *gin.Context) {
	var loginReq model.LoginReq
	if c.BindJSON(&loginReq) == nil {
		// 登陆逻辑校验  返回三个值 bool user err
		isPass, _, _ := model.LoginCheck(loginReq)
		if isPass {
			// token = service.GenerateToken(c)

			data_token := model.LoginResult{}
			response.LoginOK(data_token, c)
		} else {
			response.LoginFail(c)
		}
	} else {
		response.LoginFail(c)
	}
}
