package main

import (
	"lizi/config"
	"lizi/core"
	"lizi/middleware"
	"lizi/model"
)

var c = &config.Config{}

func init_config() {
	path := "./config/config.yaml"
	config.Parse(c, path)
}

// 初始化db
func init_db() {
	dbErr := middleware.InitMySQL(c)
	if dbErr != nil {
		panic(dbErr)
	}
	model.InitModel()
	defer middleware.DB.Close()
}

func init() {
	init_config()
	init_db()
}

func main() {
	core.Run(c)
}
