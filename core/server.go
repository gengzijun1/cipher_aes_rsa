package core

import (
	"lizi/config"
	"lizi/route"

	"github.com/gin-gonic/gin"
)

func add_router() {
	route.User(*router)
	route.TestApi(*router)
}

func Run(c *config.Config) {
	server := gin.Default()
	// add_router()
	server.Run(":" + c.Server.Port)
}
