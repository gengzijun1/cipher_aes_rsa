package config

import (
	"os"

	"gopkg.in/yaml.v2"
)

type Config struct {
	DB     DB     `yaml:"db"`
	Server Server `yaml:"server"`
	JWT    JWT    `yaml:"jwt"`
}

func ParseYaml(c *Config, path string) error {
	if f, err := os.Open(path); err != nil {
		return err
	} else {
		yaml.NewDecoder(f).Decode(c)
	}
	return nil
}

func Parse(c *Config, path string) error {
	return ParseYaml(c, path)
}
