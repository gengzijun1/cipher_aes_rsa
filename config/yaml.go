package config

type DB struct {
	Host     string `yaml:"host"`
	Port     string `yaml:"port"`
	User     string `yaml:"user"`
	Passwd   string `yaml:"passwd"`
	Database string `yaml:"database"`
}

type Server struct {
	Host string `yaml:"host"`
	Port string `yaml:"port"`
}

type AES struct {
	Password string `yaml:"password"`
	Aeskey   string `yaml:"Aeskey"`
}

type RSA struct {
	PublicFile  string `yaml:"PublicFile"`
	PrivateFile string `yaml:"PrivateFile"`
	PublicKey   string `yaml:"PublicKey"`
	PrivateKey  string `yaml:"PrivateKey"`
}

type JWT struct {
	AES AES `yaml:"aes"`
	RSA RSA `yaml:"rsa"`
}
