package service

import (
	"lizi/middleware"
)

// token生成器
func GenerateToken(data interface{}) string {
	// 构造SignKey: 签名和解签名需要使用一个值
	j := middleware.JWTKEY()
	// 根据claims生成token对象
	token, err := j.CreateToken(data, 36000, 1000)
	if err != nil {
		return err.Error()
	}
	return token
}

// token生成器
func ParseToken(token string) interface{} {
	// 构造SignKey: 签名和解签名需要使用一个值
	j := middleware.JWTKEY()
	// 根据claims生成token对象
	data, err := j.ParserToken(token)
	if err != nil {
		return err.Error()
	}
	return data
}
